Example [Middleman](https://middlemanapp.com/) website using GitLab Pages.

Learn more about GitLab Pages by visiting the
[official documentation](https://docs.gitlab.com/ee/user/project/pages/).

## GitLab CI

This project's static Pages are built by [GitLab CI/CD](https://about.gitlab.com/solutions/continuous-integration/), following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install](https://middlemanapp.com/basics/install/) Middleman
1. Generate the website: `bundle exec middleman build`
1. Preview your project: `bundle exec middleman`
1. Add content

Read more at Middleman's [documentation](https://middlemanapp.com/basics/install/).

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages and project Pages](https://docs.gitlab.com/ee/user/project/pages/introduction.html#gitlab-pages-in-projects-and-groups).

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.
